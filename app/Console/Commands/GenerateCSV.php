<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;

use Facade\FlareClient\Http\Response;
use Illuminate\Support\Facades\Storage;

class GenerateCSV extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generatecsv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $endPoints = [
            'customers' => 'https://storage.googleapis.com/neta-interviews/MJZkEW3a8wmunaLv/customers.json',
            'orders' => 'https://storage.googleapis.com/neta-interviews/MJZkEW3a8wmunaLv/orders.json',
            'items' => 'https://storage.googleapis.com/neta-interviews/MJZkEW3a8wmunaLv/items.json'
        ];

        $csvData = [];
        // Customers
        $customersData = [];
        # Making the calls
        $customerAPI = $endPoints['customers'];
        $client = new Client();
        $response = $client->request('GET', $customerAPI);

        if ($response->getStatusCode() != 200) {
            return Response('Endpoint error!');
        } else {
            $customers = json_decode($response->getBody()->getContents());
            foreach ($customers as $customerData) {
                $customerId = $customerData->id;
                $customersData[$customerId] = [
                    'firstName' => $customerData->firstName,
                    'lastName' => $customerData->lastName,
                ];
                foreach ($customerData->addresses as $addressDetails) {
                    if ($addressDetails->type == 'shipping') {
                        $customersData[$customerId]['address'] = $addressDetails->address;
                        $customersData[$customerId]['city'] = $addressDetails->city;
                        $customersData[$customerId]['zip'] = $addressDetails->zip;
                    }
                }
                $customersData[$customerId]['email'] = $customerData->email;
            }
        }

        // orders
        $ordersData = [];
        # Making the calls
        $ordersAPI = $endPoints['orders'];
        $client = new Client();
        $response = $client->request('GET', $ordersAPI);

        if ($response->getStatusCode() != 200) {
            return Response('Orders endpoint error!');
        } else {
            $orders = json_decode($response->getBody()->getContents());
            foreach ($orders as $orderData) {
                $ordersData[$orderData->id] = array_merge(
                    ['orderId' => $orderData->id, 'orderDate' => $orderData->createdAt],
                    $customersData[$orderData->customerId]
                );
            }
        }

        // Items
        # Making the calls
        $itemsAPI = $endPoints['items'];
        $client = new Client();
        $response = $client->request('GET', $itemsAPI);
        if ($response->getStatusCode() != 200) {
            return Response('Items endpoint error!');
        } else {
            $items = json_decode($response->getBody()->getContents());
            foreach ($items as $itemData) {
                $csvData[$itemData->id] = implode(",", array_merge(
                    array_slice($ordersData[$itemData->orderId], 0, 2, true),
                    [
                        'orderItemId' => $itemData->id,
                        'orderItemName' => $itemData->name,
                        'orderItemQuantity' => $itemData->quantity,
                    ],
                    array_slice($ordersData[$itemData->orderId], 2, null, true)
                )) . "\n";
            }
        }

        // Generate the csv file
        $fileContent = "orderID, orderDate, orderItemID, orderItemName, orderItemQuantity, customerFirstName, customerLastName, customerAddress, customerCity, customerZipCode, customerEmail" . "\n";
        $fileContent .= implode('', $csvData);
        Storage::put('filename.csv', $fileContent);
    }
}